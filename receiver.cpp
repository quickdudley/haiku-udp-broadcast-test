#include <DatagramSocket.h>
#include <iostream>

int main(int argc, char **argv) {
  char buffer[1024];
  BDatagramSocket s;
  BNetworkAddress bindAddress;
  bindAddress.SetToWildcard(AF_INET, 5555);
  s.Bind(bindAddress, true);
  s.SetBroadcast(true);
  BNetworkAddress fromAddress;
  size_t received = s.ReceiveFrom(buffer, sizeof(buffer) - 1, fromAddress);
  buffer[received] = 0;
  std::cout << "Received from address: " << fromAddress.ToString(false) << std::endl;
  std::cout << "Received content: " << buffer << std::endl;
}