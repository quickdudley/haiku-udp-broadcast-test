# Haiku UDP broadcast test

To run the test: run `make test`. If things are working as expected, it will
print a couple of lines to cout. If it just hangs: UDP broadcast is not working.

