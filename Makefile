.PHONY: test

all: sender receiver

CARGS = -lbe -lnetwork -lbnetapi

sender: sender.cpp
	g++ sender.cpp $(CARGS) -o sender

receiver: receiver.cpp
	g++ receiver.cpp $(CARGS) -o receiver

test: all
	./receiver & (sleep 1s; ./sender); wait