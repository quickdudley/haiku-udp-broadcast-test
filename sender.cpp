#include <DatagramSocket.h>
#include <NetworkInterface.h>
#include <NetworkRoster.h>

status_t sendBroadcast() {
  BDatagramSocket s;
  BNetworkAddress bindAddress;
  bindAddress.SetToWildcard(AF_INET, 0);
  s.Bind(bindAddress, true);
  s.SetBroadcast(true);
  auto port = s.Local().Port();
  // Check which network interfaces exist
  BNetworkRoster &roster = BNetworkRoster::Default();
  BNetworkInterface interface;
  uint32 rosterCookie = 0;
  while (roster.GetNextInterface(&rosterCookie, interface) == B_OK) {
    // Check which addresses exist for this interface
    int32 count = interface.CountAddresses();
    for (int32 i = 0; i < count; i++) {
      BNetworkInterfaceAddress address;
      if (interface.GetAddressAt(i, address) != B_OK)
        break;
      BNetworkAddress myAddress = address.Address();
      myAddress.SetPort(port);
      if (!myAddress.IsEmpty() &&
          myAddress != BNetworkAddress("::1", port)) {
        BNetworkAddress broadcastAddress = address.Broadcast();
        BString payload = myAddress.ToString(false);
        broadcastAddress.SetPort(5555);
        s.SendTo(broadcastAddress, payload.String(),
                             (size_t)payload.Length());
      }
    }
  }
  return B_OK;
}

int main(int argc, char **args) {
  sendBroadcast();
  return 0;
}
